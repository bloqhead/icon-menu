/*global module:false*/
module.exports = function( grunt ) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
			'<%= grunt.template.today("yyyy-mm-dd") %>\n' +
			'<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>',
		sass: {
			dist: {
				options: {
					require : [ 'sass-globbing' ]
				},
				files: [ {
					expand: true,
					flatten: true,
					cwd: 'assets/scss',
					src: [
						'**/*.scss',
						'!**/_*.scss'
					],
					dest: 'assets/css/',
					ext: '.css'
				} ]
			}
		},
		coffee: {
			dist: {
				files: [ {
					expand: true,
					flatten: true,
					cwd: 'assets/coffee',
					src: [ '**/*.coffee' ],
					dest: 'assets/js/',
					ext: '.js'
				} ]
			}
		},
		watch: {
			dist : {
				files: [
					'assets/scss/**/*.scss',
					'assets/coffee/**/*.coffee'
				],
				tasks: [ 'sass','coffee' ],
				options: {
					livereload: true
				}
			}
		},
		uglify : {
			dist : {
				files : {
					'assets/js/icon-menu.min.js': [
						'assets/js/icon-menu.js'
					]
				}
			}
		},
		cssmin : {
			dist : {
				files : {
					'assets/css/main.min.css': [
						'assets/css/main.css'
					]
				}
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-coffee');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.registerTask('default', ['sass','coffee']);
	grunt.registerTask('build', ['default','uglify','cssmin']);
};
