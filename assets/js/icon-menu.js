(function() {
  $('a.menu-toggle').on('click', function(e) {
    e.preventDefault();
    $('.icon-menu').toggleClass('active');
    $('.icon-menu-container').toggleClass('active');
    $(this).toggleClass('active');
    return $('body').toggleClass('icon-menu-shifted');
  });

  $('.icon-menu > li').on({
    mouseenter: function() {
      return $(this).siblings().addClass('faded');
    },
    mouseleave: function() {
      return $(this).siblings().removeClass('faded');
    }
  });

}).call(this);
