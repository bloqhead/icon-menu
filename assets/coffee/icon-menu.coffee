$('a.menu-toggle').on 'click', (e) ->
	e.preventDefault()
	$('.icon-menu').toggleClass 'active'
	$('.icon-menu-container').toggleClass 'active'
	$(@).toggleClass 'active'
	$('body').toggleClass 'icon-menu-shifted'

$('.icon-menu > li').on
	mouseenter: ->
		$(@).siblings().addClass 'faded'
	mouseleave: ->
		$(@).siblings().removeClass 'faded'